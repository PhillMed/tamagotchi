# Tamagotchi



## About

In this project, I tried to implement a **tamagotchi game** where you need to grow and take care of your plant.

The basis of the application is working. The flower grows from ***water*** and ***fertilize***. 
If the water drops to 0 or the nutrients do not arrive for some time (below zero), then the flower dies and the inscription ***Game Over*** appears

generation works quite well, but  still a crooked plant also happens.

I also tried to transfer all this to the GUI, but it turned out to be a little more complicated than I thought, so at the moment the game is running in the console and there is a window with buttons (water / fertilize / Trim the plant).

The last button aligns the top row, and also reduces the sprouts by 1 (for example 3 -> 2).
