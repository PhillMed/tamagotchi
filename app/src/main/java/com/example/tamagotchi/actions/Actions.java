package main.java.com.example.tamagotchi.actions;

import java.awt.event.ActionListener;

import static main.java.com.example.tamagotchi.actions.PassiveGameplay.fertilizerResource;
import static main.java.com.example.tamagotchi.actions.PassiveGameplay.waterResource;
import static main.java.com.example.tamagotchi.settings.Settings.massiveOfTamagotchi;
import static main.java.com.example.tamagotchi.stats.FertilizersStats.maxFertilize;
import static main.java.com.example.tamagotchi.stats.WaterStats.maxWater;

public class Actions {

    public static ActionListener watering() {
        waterResource += (maxWater/5);
        return null;
    }

    public static ActionListener fertilize() {
        fertilizerResource += (maxFertilize/5);
        return null;
    }

    public static ActionListener trimThePlant() {
        int firstLineWithThePlant = massiveOfTamagotchi.length - 1;
        for (int i = 0; i < massiveOfTamagotchi.length; i++) {
            for (int j = 0; j < massiveOfTamagotchi[i].length; j++) {
                if (massiveOfTamagotchi[i][j] > 0 && massiveOfTamagotchi[i][j] < 6) {
                    if (firstLineWithThePlant > j) {
                        firstLineWithThePlant = j;
                    }
                }
            }
            if (massiveOfTamagotchi[i][firstLineWithThePlant] == 2 || massiveOfTamagotchi[i][firstLineWithThePlant] == 3) {
                massiveOfTamagotchi[i][firstLineWithThePlant] -= 1;
            }

            if (massiveOfTamagotchi[i][firstLineWithThePlant] == 4 || massiveOfTamagotchi[i][firstLineWithThePlant] == 5) {
                massiveOfTamagotchi[i][firstLineWithThePlant] = 1;
            }
        }
        return null;
    }

    public void removeWeeds() {}

    public void changeThePlant() {}
}
