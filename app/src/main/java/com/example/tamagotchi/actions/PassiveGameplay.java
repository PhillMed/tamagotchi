package main.java.com.example.tamagotchi.actions;

import java.util.concurrent.TimeUnit;

import static main.java.com.example.tamagotchi.stats.FertilizersStats.fertilize;
import static main.java.com.example.tamagotchi.stats.WaterStats.water;

public class PassiveGameplay {
    public static int waterResource = water;
    public static int ticksForAll = 0;
    public static int fertilizerResource = fertilize;
    public static int waterEvaporation() throws InterruptedException {
        TimeUnit.SECONDS.sleep(2);
        waterResource = waterResource - 100;
        return waterResource;
    }

    public static int fertilizerEvaporation() {
        fertilizerResource = fertilizerResource - 10;
        return fertilizerResource;
    }


    public static void plantIsDie() throws InterruptedException {
        System.out.println("Game over");
//        System.out.println(3);
//        TimeUnit.SECONDS.sleep(1);
//        System.out.println(2);
//        TimeUnit.SECONDS.sleep(1);
//        System.out.println(1);
//        TimeUnit.SECONDS.sleep(1);
//        create();
//        print();
    }
}
