package main.java.com.example.tamagotchi.stats;

public interface FertilizersStats {
    int fertilize = 100;
    int maxFertilize = 10000;
    int minFertilize = 0;
}
