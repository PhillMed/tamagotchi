package main.java.com.example.tamagotchi.stats;

public interface WaterStats {
    int water = 2000;
    int maxWater = 12000;
    int minWater = 0;
}
