package main.java.com.example.tamagotchi;



import main.java.com.example.tamagotchi.gui.TamagotchiGUI;

import static main.java.com.example.tamagotchi.basic.Create.create;
import static main.java.com.example.tamagotchi.basic.Grow.growStage;
import static main.java.com.example.tamagotchi.basic.Ticks.ticksEternal;

public class MainTamagotchi {
    public static void main(String[] args) throws InterruptedException {

        create();

        TamagotchiGUI app = new TamagotchiGUI();
        app.setVisible(true);

        growStage();
        ticksEternal();


    }

}
