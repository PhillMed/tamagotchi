package main.java.com.example.tamagotchi.basic;

import java.util.Random;

import static main.java.com.example.tamagotchi.settings.Settings.massiveOfTamagotchi;

public class GenerateOfGrow {

    static void checkForGrowCenter(int i, int j) {
        if ((ezAllRight(i, j) && ezAllLeft(i, j)) && (massiveOfTamagotchi[i - 1][j] == 0 || massiveOfTamagotchi[i - 1][j] > 300)) {
            generateCenterUp(i, j);
        }
    }

    static void checkForGrowLeft(int i, int j) {
        if ((ezAllLeft(i, j)) && (massiveOfTamagotchi[i - 1][j - 1] == 0 || massiveOfTamagotchi[i - 1][j - 1] > 300)) {
            generateLeftUp(i, j);
        }
    }

    static void checkForGrowRight(int i, int j) {
        if ((ezAllRight(i, j)) && (massiveOfTamagotchi[i - 1][j + 1] == 0 || massiveOfTamagotchi[i - 1][j + 1] > 300)) {
            generateRightUp(i, j);
        }
    }

    private static void generateCenterUp(int i, int j) {
        massiveOfTamagotchi[i - 1][j] = generateGrowUp(j);
    }

    private static void generateRightUp(int i, int j) {
        massiveOfTamagotchi[i - 1][j + 1] = generateGrowRight(j);
    }

    private static void generateLeftUp(int i, int j) {
        massiveOfTamagotchi[i - 1][j - 1] = generateGrowLeft(j);
    }

    static int generateFlower() {
        return 201;
    }

    private static int generateGrowUp(int j) {
        Random random = new Random();
        int[] growForUp = {1, 2, 3};
        int[] growForUpWithoutFlower = {1, 2, 3, 6};
        if (massiveOfTamagotchi[11][j] != 0) {
            return growForUp[random.nextInt(growForUp.length)];
        } else {
            return growForUpWithoutFlower[random.nextInt(growForUpWithoutFlower.length)];
        }
    }

    private static int generateGrowLeft(int j) {
        Random random = new Random();
        int[] growForLeft = {1, 4, 6};
        int[] growForLeftWithoutFlower = {1, 4};
        if (massiveOfTamagotchi[11][j] != 0) {
            return growForLeft[random.nextInt(growForLeft.length)];
        } else {
            return growForLeftWithoutFlower[random.nextInt(growForLeftWithoutFlower.length)];
        }
    }

    private static int generateGrowRight(int j) {
        Random random = new Random();
        int[] growForRight = {1, 5, 6};
        int[] growForRightWithoutFlower = {1, 5};
        if (massiveOfTamagotchi[11][j] != 0) {
            return growForRight[random.nextInt(growForRight.length)];
        } else {
            return growForRightWithoutFlower[random.nextInt(growForRightWithoutFlower.length)];
        }
    }

    private static int ezLeft(int i, int j) {
        return massiveOfTamagotchi[i][j - 1];
    }

    private static int ezRight(int i, int j) {
        return massiveOfTamagotchi[i][j + 1];
    }

    private static boolean ezAllRight(int i, int j) {
        return ezRight(i, j) == 0 || ezRight(i, j) == 1 || ezRight(i, j) == 5 || ezRight(i, j) > 200;
    }

    private static boolean ezAllLeft(int i, int j) {
        return ezLeft(i, j) == 0 || ezLeft(i, j) == 1 || ezLeft(i, j) == 4 || ezLeft(i, j) > 200;
    }
}
