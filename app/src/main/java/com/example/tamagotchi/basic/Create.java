package main.java.com.example.tamagotchi.basic;

import lombok.Getter;
import lombok.Setter;

import java.util.Random;

@Setter
@Getter
public class Create {

    public static int[][] create() {

        int massiveY = 15;
        int massiveX = 15;
        Random random = new Random();

        int[][] initTamagotchiMassive = new int[massiveY][massiveX];

        for (int j = 0; j < massiveX; j++) {
            initTamagotchiMassive[12][j] = random.nextInt(6) + 1008;
        }

        for (int j = 0; j < massiveX; j++) {
            initTamagotchiMassive[13][j] = random.nextInt(13) + 1001;
        }

        for (int j = 0; j < massiveX; j++) {
            initTamagotchiMassive[14][j] = random.nextInt(8) + 1001;
        }

        initTamagotchiMassive[13][7] = random.nextInt(5) + 1;
        return initTamagotchiMassive;

    }


}
