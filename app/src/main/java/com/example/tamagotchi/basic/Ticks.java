package main.java.com.example.tamagotchi.basic;

import static main.java.com.example.tamagotchi.actions.PassiveGameplay.*;
import static main.java.com.example.tamagotchi.basic.Grow.growStage;
import static main.java.com.example.tamagotchi.basic.Print.printArray;
import static main.java.com.example.tamagotchi.stats.WaterStats.minWater;

public class Ticks {
    public static int ticksEternal() throws InterruptedException {

        int ticksForAll = 0;
        int ticksForGrow = 0;

        while (true) {
            printArray();
            ticksForAll = ticksForAll + 1;
            System.out.println("ticksForAll " + ticksForAll);
            if (waterResource == minWater) {
                plantIsDie();
                break;
            }

            ticksForGrow++;
            waterEvaporation();
            fertilizerEvaporation();

            System.out.println("ticksForGrow " + ticksForGrow);
            if (ticksForGrow == 10) {
                growStage();
                printArray();
                ticksForGrow = 0;
            }

        }

        return ticksForAll;
    }
}

