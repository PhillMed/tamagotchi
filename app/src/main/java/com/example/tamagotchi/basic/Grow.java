package main.java.com.example.tamagotchi.basic;

import static main.java.com.example.tamagotchi.actions.PassiveGameplay.waterEvaporation;
import static main.java.com.example.tamagotchi.basic.GenerateOfGrow.*;
import static main.java.com.example.tamagotchi.settings.Settings.massiveOfTamagotchi;

public class Grow {
    public static int[][] growStage() throws InterruptedException {
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 15; j++) {
                if (massiveOfTamagotchi[1][j] < 6 && massiveOfTamagotchi[1][j] > 0) {
                    massiveOfTamagotchi[1][j] = generateFlower();
                } else if (massiveOfTamagotchi[i][1] < 6 && massiveOfTamagotchi[i][1] > 0) {
                    massiveOfTamagotchi[i][1] = generateFlower();
                } else if (massiveOfTamagotchi[i][14] < 6 && massiveOfTamagotchi[i][14] > 0) {
                    massiveOfTamagotchi[i][14] = generateFlower();
                }

                if (massiveOfTamagotchi[i][j] == 6) {
                    massiveOfTamagotchi[i][j] = generateFlower();
                }

                if (massiveOfTamagotchi[i][j] == 202) {
                    massiveOfTamagotchi[i][j]++;
                }

                if (massiveOfTamagotchi[i][j] == 201) {
                    massiveOfTamagotchi[i][j]++;
                }

                if (massiveOfTamagotchi[i][j] == 1) {
                    checkForGrowCenter(i, j);

                } else if (massiveOfTamagotchi[i][j] == 2) {
                    checkForGrowLeft(i, j);
                    checkForGrowRight(i, j);

                } else if (massiveOfTamagotchi[i][j] == 3) {
                    checkForGrowLeft(i, j);
                    checkForGrowCenter(i, j);
                    checkForGrowRight(i, j);

                } else if (massiveOfTamagotchi[i][j] == 4) {
                    checkForGrowLeft(i, j);

                } else if (massiveOfTamagotchi[i][j] == 5) {
                    checkForGrowRight(i, j);
                }

            }
        }
        return massiveOfTamagotchi;
    }

}
