package main.java.com.example.tamagotchi.basic;

import lombok.Getter;
import lombok.Setter;

import static main.java.com.example.tamagotchi.actions.PassiveGameplay.fertilizerResource;
import static main.java.com.example.tamagotchi.actions.PassiveGameplay.waterResource;
import static main.java.com.example.tamagotchi.settings.Settings.isNeedSpace;
import static main.java.com.example.tamagotchi.settings.Settings.massiveOfTamagotchi;


@Getter
@Setter
public class Print {
    public static void printArray() {
        int[][] msv = massiveOfTamagotchi;

        String space;
        if (isNeedSpace) {
            space = " ";
        } else {
            space = "";
        }


        for (int i = 0; i < msv.length; i++, System.out.println()) {
            for (int j = 0; j < msv[i].length; j++) {
                if (msv[i][j] == 0)
                    System.out.print(" " + space);
                else if (msv[i][j] == 1)
                    System.out.print('|' + space);
                else if (msv[i][j] == 2)
                    System.out.print("V" + space);
                else if (msv[i][j] == 3)
                    System.out.print("W" + space);
                else if (msv[i][j] == 4)
                    System.out.print("\\" + space);
                else if (msv[i][j] == 5)
                    System.out.print("/" + space);
                else if (msv[i][j] == 201)
                    System.out.print("@" + space);
                else if (msv[i][j] == 202)
                    System.out.print("๑" + space);
                else if (msv[i][j] == 203)
                    System.out.print("ύ" + space);
                else if (msv[i][j] == 1001)
                    System.out.print("▲" + space);
                else if (msv[i][j] == 1002)
                    System.out.print("△" + space);
                else if (msv[i][j] == 1003)
                    System.out.print("▶" + space);
                else if (msv[i][j] == 1004)
                    System.out.print("▷" + space);
                else if (msv[i][j] == 1005)
                    System.out.print("◆" + space);
                else if (msv[i][j] == 1006)
                    System.out.print("▽" + space);
                else if (msv[i][j] == 1007)
                    System.out.print("●" + space);
                else if (msv[i][j] == 1008)
                    System.out.print("◂" + space);
                else if (msv[i][j] == 1009)
                    System.out.print("." + space);
                else if (msv[i][j] == 1010)
                    System.out.print("-" + space);
                else if (msv[i][j] == 1011)
                    System.out.print("," + space);
                else if (msv[i][j] == 1012)
                    System.out.print("*" + space);
                else if (msv[i][j] == 1013)
                    System.out.print("·" + space);
                else {
                    System.out.print(" " + space);
                }

            }
        }
        System.out.println("waterResource " + waterResource);
        System.out.println("fertilizerResource " + fertilizerResource);
    }
}
