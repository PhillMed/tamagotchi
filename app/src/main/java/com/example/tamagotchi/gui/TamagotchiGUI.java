package main.java.com.example.tamagotchi.gui;

import javax.swing.*;
import java.awt.*;

import static main.java.com.example.tamagotchi.actions.Actions.*;

public class TamagotchiGUI extends JFrame {
    public TamagotchiGUI() {
        super("Tamagotchi plant");

        ImageIcon mainIcon = new ImageIcon("flower.png");
        this.setIconImage(mainIcon.getImage());

        ImageIcon settingsIcon = new ImageIcon("SettingsButton.png");

        JButton button_water = new JButton("Water");
        button_water.setFocusable(false);
        JButton button_fertilize = new JButton("Fertilize");
        button_fertilize.setFocusable(false);
        JButton button_trimThePlant = new JButton("Trim the plant");
        button_trimThePlant.setFocusable(false);

        button_water.addActionListener(e -> watering());
        button_fertilize.addActionListener(e -> fertilize());
        button_trimThePlant.addActionListener(e -> trimThePlant());

        JButton button_settings = new JButton();
        button_settings.setFocusable(false);
        button_settings.setIcon(settingsIcon);

        JPanel panelOfGeneration = new JPanel();
        panelOfGeneration.setBackground(Color.BLACK);
        panelOfGeneration.setPreferredSize(new Dimension(200,300));

        JPanel panelOfScore = new JPanel();
        panelOfScore.setBackground(Color.GRAY);
        panelOfScore.setPreferredSize(new Dimension(200,50));

        JPanel panelOfBars = new JPanel();
        panelOfBars.setBackground(Color.cyan);
        panelOfBars.setPreferredSize(new Dimension(220,50));

        JPanel panelOfSettings = new JPanel();
        panelOfSettings.setBackground(Color.black);
        panelOfSettings.setPreferredSize(new Dimension(45,45));
        panelOfSettings.add(button_settings);

        JPanel panelOfButtons1 = new JPanel();
        panelOfButtons1.add(button_water);
        panelOfButtons1.add(button_fertilize);
        panelOfButtons1.add(button_trimThePlant);
        panelOfButtons1.setBackground(Color.RED);
        panelOfButtons1.setPreferredSize(new Dimension(280,35));

        JPanel panelOfButtons2 = new JPanel();
        panelOfButtons2.setBackground(Color.pink);
        panelOfButtons2.setPreferredSize(new Dimension(280,35));

        JPanel panelOfStats = new JPanel();
        panelOfStats.setBackground(Color.orange);
        panelOfStats.setPreferredSize(new Dimension(280,230));
//
//
//        JPanel panelLeftPart = new JPanel();
//        panelLeftPart.setBackground(Color.MAGENTA);
//        panelLeftPart.setPreferredSize(new Dimension(100,100));
//        panelLeftPart.add(panelOfGeneration);
//        panelLeftPart.add(panelOfScore);
//
//        JPanel panelAll = new JPanel();
//        panelAll.setBackground(Color.cyan);
//        panelAll.setLayout(new BorderLayout());
//        panelAll.setPreferredSize(new Dimension(100,100));
//        panelAll.add(panelLeftPart);
//
//
//        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        this.setLayout(null);
//        this.setSize(500, 400);
//        this.setVisible(true);
//        this.add(panelAll, BorderLayout.CENTER);
//        this.add(panelOfButtons1);
//        this.add(panelOfButtons2);
//        this.add(panelOfBars);
//        this.add(panelOfSettings);
//        this.add(panelOfStats);
//
//    }
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(520, 400);
        this.setLayout(new BorderLayout(10,10));
        this.setVisible(true);

        JPanel panelLeftPart = new JPanel();
        JPanel panelRightPart = new JPanel();
        JPanel panelRightPartUP = new JPanel();
        JPanel panelRightPartDOWN = new JPanel();
        JPanel panelAll = new JPanel();
        JPanel panelOfButtons = new JPanel();

        panelLeftPart.setBackground(Color.red);
        panelRightPart.setBackground(Color.orange);
        panelRightPartUP.setBackground(Color.green);
        panelRightPartDOWN.setBackground(Color.yellow);
        panelAll.setBackground(Color.blue);

        panelAll.setLayout(new BorderLayout());
        panelRightPart.setLayout(new BorderLayout());
        panelOfButtons.setLayout(new BorderLayout());


        panelLeftPart.setPreferredSize(new Dimension(200,400));
        panelRightPart.setPreferredSize(new Dimension(300,400));
        panelRightPartUP.setPreferredSize(new Dimension(300, 60));
        panelRightPartDOWN.setPreferredSize(new Dimension(300,300));

        //------------- slider ------------------------

        Panel sliderOfTimePanel = new Panel();
        Label sliderOfTimeLabel = new Label();
        JSlider timeSpeed = new JSlider(0,100,75);

        sliderOfTimePanel.add(timeSpeed);
        sliderOfTimePanel.add(sliderOfTimeLabel);

        panelOfStats.add(sliderOfTimePanel);

        timeSpeed.setPaintTicks(true);
        timeSpeed.setMinorTickSpacing(10);

        timeSpeed.setPaintTrack(true);
        timeSpeed.setMajorTickSpacing(25);

        timeSpeed.setPaintLabels(true);
        timeSpeed.setFont(new Font("MV Boli", Font.PLAIN,12));

        sliderOfTimeLabel.setText("time speed: " + timeSpeed.getValue());
        sliderOfTimeLabel.setFont(new Font("MV Boli", Font.PLAIN,13));


        //------------- sub panels --------------------

        panelLeftPart.add(panelOfGeneration, BorderLayout.NORTH);
        panelLeftPart.add(panelOfScore, BorderLayout.SOUTH);

        panelRightPartUP.add(panelOfBars, BorderLayout.WEST);
        panelRightPartUP.add(button_settings, BorderLayout.EAST);

        panelOfButtons.add(panelOfButtons1, BorderLayout.NORTH);
        panelOfButtons.add(panelOfButtons2, BorderLayout.SOUTH);

        panelRightPartDOWN.add(panelOfButtons, BorderLayout.NORTH);
        panelRightPartDOWN.add(panelOfStats, BorderLayout.SOUTH);

        panelRightPart.add(panelRightPartUP, BorderLayout.NORTH);
        panelRightPart.add(panelRightPartDOWN, BorderLayout.SOUTH);

        panelAll.setLayout(new BorderLayout());

        panelAll.add(panelLeftPart,BorderLayout.WEST);
        panelAll.add(panelRightPart,BorderLayout.EAST);

        //------------- sub panels --------------------

        this.add(panelAll,BorderLayout.CENTER);
    }
}

